package HomeWork10;

public class StringBuilder {
    private String string;
    private int num;

    public StringBuilder(String text){
        this.string = text;
    }

    public StringBuilder(int num){
        this.num = num;
    }

    public String append(String appendText){
        return this.string += appendText;
    }

    @Override
    public String toString() {
        return this.string;
    }
}
