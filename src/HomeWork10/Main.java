package HomeWork10;

public class Main {
    public static void main(String[] args) {
        Human human1 = new Human("Stive", "Morgan", "Russia", "Male", "Atheist",
                "KAI", 23, 2000);
        Human human2 = new Human("Piter", "Miller", "Ukraine", "Male", "Atheist",
                "KAI", 20, 4000);
        Human human3 = new Human("Stive", "Morgan", "Russia", "Male", "Atheist",
                "KAI", 23, 2000);
        Human human4 = new Human("Stive", "Morgan", "Russia", "Male", "Atheist",
                "KAI", 23, 2001);

        human1.print();
        human2.print();
        human3.print();
        human4.print();

        System.out.println("Human1 == Human3 ? " + human1.equals(human3));
        System.out.println("Human1 == Human2 ? " + human1.equals(human2));
        System.out.println("Human2 == Human3 ? " + human2.equals(human3));
        System.out.println("Human1 == Human4 ? " + human1.equals(human4));

        StringBuilder strBuilderCustom = new StringBuilder("Home");
        System.out.println(strBuilderCustom.append("Work"));
        System.out.println(strBuilderCustom.toString());
    }
    
    
}
