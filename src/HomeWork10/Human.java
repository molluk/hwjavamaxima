package HomeWork10;

import java.util.Locale;
import java.util.Objects;

public class Human {
    private String name;
    private String lastName;
    private String country;
    private String gender;
    private String religion;
    private String university;
    private int age;
    private long money;

    public Human(String name, String lastName, String country, String gender,
                 String religion, String university, int age, int money) {
        this.name = name;
        this.lastName = lastName;
        this.country = country;
        this.gender = gender;
        this.religion = religion;
        this.university = university;
        this.age = age;
        this.money = money;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Human human = (Human) obj;

        if (this.name.toLowerCase() == human.getName().toLowerCase() ||
                this.lastName.toLowerCase() == human.getLastName().toLowerCase() ||
                this.country.toLowerCase() == human.getCountry().toLowerCase() ||
                this.gender.toLowerCase() == human.getGender().toLowerCase() ||
                this.religion.toLowerCase() == human.getReligion().toLowerCase() ||
                this.university.toLowerCase() == human.getUniversity().toLowerCase() ||
                this.money != human.getMoney() ||
                this.age != human.getAge()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", gender='" + gender + '\'' +
                ", religion='" + religion + '\'' +
                ", university='" + university + '\'' +
                ", age=" + age +
                ", money=" + money +
                '}';
    }

    public void print() {
        System.out.println(this.toString());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }
}
