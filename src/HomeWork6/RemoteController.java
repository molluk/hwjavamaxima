package HomeWork6;

public class RemoteController {
    private String modelRemoteController;
    private String modelTV;

    public RemoteController(String modelTV) {
        this.modelTV = modelTV;
        modelRemoteController = modelTV + " remote controller";
    }

    public String getModelRemoteController() {
        return modelRemoteController;
    }

    public void on(int channelNumber) {
        System.out.println(Channel.getChannelAndProgram(channelNumber));
    }
}
