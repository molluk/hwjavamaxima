package HomeWork6;

public class Channel {
    private static String[] nameChannel = new String[]{"Первый канал", "Россия 1", "МАТЧ! ТВ", "НТВ", "5 канал", "Россия К",
            "Россия 24", "Карусель", "ОТР", "ТВЦ", "РЕН-ТВ", "Спас", "СТС", "Домашний", "ТВ3", "Звезда", "Мир", "ТНТ",
            "МУЗ-ТВ", "Тверской проспект", "Югра", "Россия 1 HD", "Че", "2X2", "СТС Love", "Ю", "Первый канал HD",
            "Союз", "8 Канал", "МАТЧ! ТВ HD", "ТНТ-4", "ЮТВ", "Fox Life", "Fox", "Paramount Comedy HD", "НСТВ",
            "Русский иллюзион", "ЕвроКино", "Иллюзион+", "Русский бестселлер", "Sony Sci-Fi", "Sony HD	Sony Turbo",
            "Русский роман HD", "Кино ТВ", "Zee TV", "Феникс+Кино", "A1", "A2", "Киносерия", "Кинокомедия", "Киномикс",
            "Spike", "Fox HD", "Fox Life HD", "Мужское кино", "Радость Моя", "Disney", "Gulli girl", "Nickelodeon",
            "Nickelodeon Junior", "Cartoon Network", "Детский	Мульт", "ANI", "Boomerang", "JimJam", "Мультимания",
            "Мультик HD", "Малыш", "Baby TV", "Тлум HD", "Детский мир", "Ginger HD", "Пингвин ЛОЛО", "Рыжий",
            "Nickelodeon HD", "TiJi", "МАТЧ! Наш спорт", "МАТЧ! Игра", "Eurosport HD", "МАТЧ! Арена", "Наш Футбол",
            "Наш Футбол HD", "МАТЧ! Боец", "Extreme Sports Channel", "Футбол", "Авто 24", "Eurosport 2 HD", "КХЛ ТВ",
            "Авто Плюс", "Познавательные 30", "TOPSHOP", "SHOP 24", "Мама", "Первый образовательный", "Большая Азия",
            "Shop&Show", "Ювелирочка", "Shopping live", "RTG TV", "National Geographic", "NatGeo Wild", "Моя Планета",
            "Охотник и рыболов", "Discovery Science HD", "Discovery HD", "Мужской", "English Club", "Ocean TV",
            "НАНО ТВ", "Доктор	24", "Техно", "Наука 2.0", "Оружие", "История	Eureka HD", "Galaxy",
            "Travel+Adventure HD", "National Geographic HD", "NatGeo Wild HD", "RTG HD", "Пятница", "Мультимузыка",
            "Точка ТВ", "Fashion TV", "Еда Премиум", "Живи ТВ", "Luxury World", "Юмор Box", "Ля-минор ТВ", "Кухня ТВ",
            "Сарафан ТВ", "Загородный", "TLC HD", "Animal Planet HD", "КВН ТВ", "Travel Channel HD", "Fine Living",
            "Food Network HD", "Ностальгия", "Загородная жизнь", "ID Investigation Discovery", "DTX", "Game Show",
            "Outdoor channel HD", "Mezzo HD", "Fashion one HD	HD Life", "Супер", "RT HD", "Про Бизнес", "РБК",
            "World Business Channel", "RTД HD", "Медиаметрикс", "Москва 24", "Евроновости", "360° Подмосковье",
            "Music Box TV", "Music Box RU", "ТНТ MUSIC", "Bridge TV", "BRIDGE TV", "Русский Хит", "BRIDGE TV DANCE",
            "RU.TV", "Europa plus TV", "Наше ТВ", "О2 ТВ", "VH1", "VH1 Classic", "MTV Hits", "MTV Dance", "MTV Rocks",
            "MTV HD", "MTV Live HD", "BRIDGE TV CLASSIC", "BRIDGE HD", "France 24", "DW TV", "TV5 MONDE", "БелРос",
            "Мир24", "Мир HD", "ТНВ - Татарстан", "CNN", "ТНВ Планета", "Телеканал «Майдан»", "ACB TV", "Беларусь 24",
            "Интер+", "NHK World", "O la la", "Русская ночь", "Playboy TV", "Brazzers Europe", "Планета HD", "Доктор HD"};

    static String getChannel(int channelNumber) {
        return nameChannel[channelNumber];
    }

    static String getChannelAndProgram(int channelNumber) {
        return "Channel name: " + getChannel(channelNumber) + "\nProgram: " + Program.program(channelNumber);
    }

    static int getNumberOfChannels() {
        return nameChannel.length - 1;
    }
}
