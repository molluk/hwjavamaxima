package HomeWork6;

public class TV {
    private String modelTV;
    private RemoteController remoteController;

    public TV(String model) {
        this.modelTV = model;
        remoteController = new RemoteController(model);
    }

    public void setModelTV(String modelTV) {
        this.modelTV = modelTV;
    }

    public String getModelTV() {
        return modelTV;
    }

    public RemoteController getRemoteController() {
        return remoteController;
    }
}
