package HomeWork4;

import java.util.Arrays;

class HomeWork4{
    public static void main(String[] args) {
        System.out.println(parse(new int[]{4, 5, 7}));
        int[] array = new int []{25, 36, 36, 46, 36, 35, 36, 35};
        System.out.println(commonNumber(array));
    }

    public static int parse(int[] array){
        int num = 0;
        for (int i = 0; i < array.length; i++){
            num += array[i];
            if (i < array.length - 1)
                num *= 10;
        }
        return num;
    }

    public static int commonNumber(int[] array){
        int saveNum = 0;
        shellSort(array);
        for (int i = 1, x = 0, y = 0; i < array.length; i++){
            if (array[i] == array[i - 1]){
                y++;
            }else{
                if(y > x){
                    x = y;
                    y = 0;
                    saveNum = array[i - 1];
                }
            }
        }
        return saveNum;
    }

    public static void shellSort(int[] array){
        int s = array.length / 2;
        while (s >= 1){
            for (int right = 0; right < array.length; right++){
                for (int i = right - s; i >= 0; i -= s){
                    if (array[i] > array[i + s]){
                        swap(array, i, i + s);
                    }
                }
            }
            s /= 2;
        }
    }

    public static void swap(int[] num, int x, int y){
        int t = num[x];
        num[x] = num[y];
        num[y] = t;
    }
}