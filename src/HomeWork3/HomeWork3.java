package HomeWork3;

import java.util.Arrays;
import java.util.Scanner;

class Math {
    public static void main(String[] args) {
        System.out.println("\n\tTASK 2");

        System.out.println("Paste you num:");
        Scanner scanner = new Scanner(System.in);
        String[] textArray = scanner.nextLine().split(" ");
        int[] num = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            num[i] = Integer.parseInt(textArray[i]);
        }

        int resultNum = 0;
        for (int i = 0; i < num.length; i++) {
            int sum = 0;
            while (num[i] > 0) {
                sum += num[i] % 10;
                num[i] /= 10;
            }
            if (sum < 18) {
                resultNum++;
            }
        }
        System.out.println("Number of digits with sum of digits less than 18 = " + resultNum);

        //TASK 3.1
        System.out.println("\n\tTASK 3.1");
        int number = 987654321;
        System.out.println("Num = " + number);
        System.out.println("Minimum digit = " + minDigit(number));

        //TASK 3.2
        System.out.println("\n\tTASK 3.2");
        number = 5;
        System.out.println("Num = " + number);
        System.out.println("Is the number even? " + isEven(number));

        //TASK 3.3
        System.out.println("\n\tTASK 3.3");
        number = 234;
        System.out.println("Num = " + number);
        System.out.println("Average = " + getDigitsAverage(number));

        //TASK 3.4
        System.out.println("\n\tTASK 3.4");
        int[] numArray = new int[]{13, 24, 53, 26, 45, 767, 94, 34, 55, 91, 23};
        System.out.println("NumArray = " + Arrays.toString(numArray));
        unknownQuantity(numArray);
    }

    //TASK 3.1
    private static int minDigit(int number) {
        int result = number % 10;
        for (int i = 0; number > 0; i++) {
            if (number % 10 < result) {
                result = number % 10;
            }
            number /= 10;
        }
        return result;
    }

    //TASK 3.2
    private static boolean isEven(int number) {
        return number % 2 == 0;
    }

    //TASK 3.3
    private static double getDigitsAverage(int number) {
        double sum = 0;
        for (int i = 1; number > 0; i++) {
            if (number / 10 == 0) {
                sum += number % 10;
                number = number / 10;
                sum = sum / i;
            } else {
                sum += number % 10;
                number = number / 10;
            }
        }
        return sum;
    }

    //TASK 3.4
    private static void unknownQuantity(int[] num) {
        for (int i = 0; i < num.length && num[i] != -1; i++) {
            System.out.print("num[" + i + "] = " + num[i]);
            if (isEven(num[i])) {
                System.out.println("\tMinimum digit = " + minDigit(num[i]));
            } else {
                System.out.println("\tAverage = " + getDigitsAverage(num[i]));
            }
        }
    }

}
