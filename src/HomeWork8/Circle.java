package HomeWork8;

public class Circle extends GeometricFigures implements Scale {
    private final double DEF_RADIUS = 5;
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
        this.radius = DEF_RADIUS;
    }

    public double getArea() {
        return Math.PI * (radius * radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public String getAllParams() {
        return "Circle:\n" + "\tRadius = " + this.getRadius() +
                "\n\tArea = " + this.getArea() + "\n\tPerimeter = " + this.getPerimeter();
    }

    @Override
    public void zoomIn(double zoom) {
        this.setRadius(this.getRadius() * Math.sqrt(zoom));
    }

    @Override
    public void zoomOut(double zoom) {
        this.setRadius(this.getRadius() / Math.sqrt(zoom));
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

}
