package HomeWork8;

public class Ellipse extends GeometricFigures implements Scale {
    private double xAxis;
    private double yAxis;

    public Ellipse(double xAxis, double yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public Ellipse() {
        this.xAxis = 10;
        this.yAxis = 5;
    }

    public double getArea() {
        return xAxis * yAxis * Math.PI;
    }

    public double getPerimeter() {
        return 4 * ((Math.PI * xAxis * yAxis + ((xAxis - yAxis) * (xAxis - yAxis))) / (xAxis + yAxis));
    }

    public String getAllParams() {
        return "Ellipse:\n" + "\tX axis = " + this.getXaxis() + "\n\tY axis = " + this.getYaxis() +
                "\n\tArea = " + this.getArea() + "\n\tPerimeter = " + this.getPerimeter();
    }

    @Override
    public void zoomIn(double zoom) {
        this.setXaxis(this.getXaxis() * Math.sqrt(zoom));
        this.setYaxis(this.getYaxis() * Math.sqrt(zoom));
    }

    @Override
    public void zoomOut(double zoom) {
        this.setXaxis(this.getXaxis() / Math.sqrt(zoom));
        this.setYaxis(this.getYaxis() / Math.sqrt(zoom));
    }

    public void setXaxis(double xAxis) {
        this.xAxis = xAxis;
    }

    public double getXaxis() {
        return this.yAxis;
    }

    public void setYaxis(double yAxis) {
        this.yAxis = yAxis;
    }

    public double getYaxis() {
        return this.xAxis;
    }
}
