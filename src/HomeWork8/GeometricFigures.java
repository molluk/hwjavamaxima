package HomeWork8;

public class GeometricFigures {

    public GeometricFigures() {
    }

    public double getEllipseArea(double xAxis, double yAxis) {
        return (xAxis / 2) * (yAxis / 2) * Math.PI;
    }


    public double getEllipsePerimeter(double xAxis, double yAxis) {
        return 4 * ((Math.PI * xAxis * yAxis + ((xAxis - yAxis) * (xAxis - yAxis))) / (xAxis + yAxis));
    }

    public double getCircleArea(double radius) {
        return Math.PI * (radius * radius);
    }

    public double getCirclePerimeter(double radius){
        return 2 * Math.PI * radius;
    }

    public double getRectangleArea(double xAxis, double yAxis){
        return xAxis * yAxis;
    }

    public double getRectanglePerimeter(double xAxis, double yAxis){
        return 2 * (xAxis + yAxis);
    }

    public double getSquareArea(double axis){
        return axis * axis;
    }

    public double getSquarePerimeter(double axis){
        return 4 * axis;
    }
}
