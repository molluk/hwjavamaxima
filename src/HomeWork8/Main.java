package HomeWork8;

public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(10, 5);
        Circle circle = new Circle(10);
        Rectangle rectangle = new Rectangle(10, 5);
        Square square = new Square(10);

        characteristicsFigures(ellipse, circle, rectangle, square);

        System.out.println("\n\tZOOM IN:");

        ellipse.zoomIn(2);
        circle.zoomIn(2);
        rectangle.zoomIn(2);
        square.zoomIn(2);

        characteristicsFigures(ellipse, circle, rectangle, square);

        System.out.println("\n\tZOOM OUT:");
        ellipse.zoomOut(4);
        circle.zoomOut(4);
        rectangle.zoomOut(4);
        square.zoomOut(4);

        characteristicsFigures(ellipse, circle, rectangle, square);
    }

    public static void characteristicsFigures(Ellipse ellipse, Circle circle, Rectangle rectangle, Square square){
        System.out.println(ellipse.getAllParams());
        System.out.println(circle.getAllParams());
        System.out.println(rectangle.getAllParams());
        System.out.println(square.getAllParams());
    }
}
