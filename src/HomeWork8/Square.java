package HomeWork8;

public class Square extends GeometricFigures implements Scale {
    private double xAxis;

    public Square(double axis) {
        this.xAxis = axis;
    }

    public Square() {
        this.xAxis = 5;
    }

    protected double getArea() {
        return xAxis * xAxis;
    }

    protected double getPerimeter() {
        return 4 * xAxis;
    }

    public String getAllParams() {
        return "Square:\n" + "\tSide = " + this.getSide() +
                "\n\tArea = " + this.getArea() + "\n\tPerimeter = " + this.getPerimeter();
    }

    @Override
    public void zoomIn(double zoom) {
        this.setSide(this.getSide() * Math.sqrt(zoom));
    }

    @Override
    public void zoomOut(double zoom) {
        this.setSide(this.getSide() / Math.sqrt(zoom));
    }

    public void setSide(double axis) {
        this.xAxis = axis;
    }

    public double getSide() {
        return this.xAxis;
    }
}
