package HomeWork8;

public interface Scale {
    public void zoomIn(double zoom);
    public void zoomOut(double zoom);
}
