package HomeWork9;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        /**
         *  Разбор обычного числа int
         */
        int num = 102030;

        NumbersProcess numbersProcess = number -> {
            int reverce = 0;
            while (number != 0) {
                reverce += (number % 10) % 2 == 0 ? (number % 10) : ((number % 10) - 1 > 0 ? (number % 10) : 0);
                if (reverce % 10 != 0) reverce *= 10;
                number /= 10;
            }
            return reverce / 10;
        };
        System.out.println(numbersProcess.process(num));

        /**
         *  Разбор String
         */
        String text = "a1b2c3d4e5f6g7h8i9j10k11l12m13n14o15p16q17r18s19t20u21v22w23x24y25z26";

        StringsProcess stringsProcess = process -> {
            char[] chars = process.toCharArray();
            for (int i = 0; i <= chars.length / 2; i++) {
                char letter = chars[i];
                chars[i] = chars[chars.length - 1 - i];
                chars[chars.length - 1 - i] = letter;
            }
            process = new String(chars);
            return process.replaceAll("[0-9]", "").toUpperCase();
        };
        System.out.println(stringsProcess.process(text));

        /**
         *  Разбор массива int[]
         */
        int[] array = {1, 2, 0, 3, 0, 4, 5, 6, 0, 0, 0, 0};

        NumbersArrayProcess numbersArrayProcess = number -> {
            int size = 0;
            //  Перемещаем 0 вправо
            for(int i = 0; i < number.length; i++){
                if(number[i] % 2 != 0){
                    number[i] = number[i] - 1 > 0 ? number[i] - 1 : 0;
                }
                if(number[i] != 0){
                    size++;
                }
            }
            int item = 0;
            for(int i = 0; i < number.length; i++){
                if(number[i] != 0){
                    number[item] = number[i];
                    item++;
                }
            }
            int[] arrayTemp = new int[size];
            for(int i = 0; i < size; i++){
                arrayTemp[i] = number[i];
            }
            number = arrayTemp;
            return number;
        };
        array = numbersArrayProcess.process(array);
        System.out.println(Arrays.toString(array));

        /**
         *  Само задание
         */

        System.out.println(numbersProcess.process(102030));
        System.out.println(stringsProcess.process("a1b2c3d4e5f6g7h8i9j10k11l12m13n14o15p16q17r18s19t20u21v22w23x24y25z26"));
        System.out.println(Arrays.toString(numbersArrayProcess.process(new int[]{1, 2, 0, 3, 0, 4, 5, 6, 0, 0, 0, 0})));
    }
}
