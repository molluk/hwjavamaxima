package HomeWork9;

public class NumbersAndStringProcessor{
    private int[] array;
    private int num;
    private String text;

    NumbersAndStringProcessor(int array[]) {
        this.array = array;
    }

    NumbersAndStringProcessor(int num) {
        this.num = num;
    }

    NumbersAndStringProcessor(String text) {
        this.text = text;
    }

    public int[] process(NumbersArrayProcess numbersArrayProcess) {
        this.array = numbersArrayProcess.process(this.array);
        return array;
    }

    public String process(StringsProcess stringsProcess) {
        this.text = stringsProcess.process(this.text);
        return null;
    }

    public int process(NumbersProcess numbersProcess){
        this.num = numbersProcess.process(this.num);
        return num;
    }

}
