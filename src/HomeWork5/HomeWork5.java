package HomeWork5;

import java.util.Random;
import java.util.Scanner;

public class HomeWork5 {
    private static final Scanner scanner = new Scanner(System.in);
    private static final Random random = new Random();

    public static void main(String[] args) {
        System.out.println("\tTASK 1.1");
        System.out.print("Array length: ");
        int[] array = arrayRandom(scanner.nextInt());   //TASK 1.1
        arrayPrint(array);
        System.out.println("\tTASK 1.2");
        arrayCopyLessThanThree(array);  //TASK 1.2
        System.out.println("\tTASK 1.3");
        matrixSum();    //TASK 1.3
        System.out.println("\tTASK 1.4");
        System.out.print("k = ");
        arraySumOfElementsToPoint(array, scanner.nextInt());
        System.out.println("\tTASK 1.5");
        arrayXY(array, 0, 5);
    }

    //TASK 1.1
    public static int[] arrayRandom(int length) {

        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(500 + 1);
        }
        return array;
    }

    //TASK 1.2
    public static void arrayCopyLessThanThree(int[] array) {
        int[] arrayClone = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            arrayClone[i] = array[i];
            if (arrayClone[i] >= 100 && arrayClone[i] <= 999) {
                int num = 0;
                int del = arrayClone[i];
                for (int j = 0; j < 3; j++, num *= 10, del /= 10) {
                    num += del % 10;
                }
                arrayClone[i] += num / 10;
            }
        }
        arrayPrint(array, arrayClone);
    }

    //TASK 1.3
    public static void matrixSum() {
        System.out.print("Insert matrix size, x = ");
        int x = scanner.nextInt();
        System.out.print("Insert matrix size, y = ");
        int y = scanner.nextInt();
        int[][] matrix = new int[x][y];
        System.out.println("Insert matrix: ");
        for (int i = 0; i < x; i++) {
            char[] sim = scanner.nextLine().toCharArray();
            for (int j = 0; j < y; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }
        System.out.println("=-._.-=-._.-=-._.-=-._.-=-._.-=-._.-=-._.-=");
        int sum = 0;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(matrix[i][j] + " ");
                if (i == j) {
                    sum += matrix[i][j];
                }
            }
            System.out.println();
        }
        System.out.println("The sum of the elements on the main diagonal of the matrix: " + sum);
    }

    //TASK 1.4.1
    public static void arraySumOfElementsToPoint(int[] array, int k) {
        int num = 0;
        for (int i = 0; i < array.length && i <= k && k <= array.length - 1; i++) {
            num += array[i];
        }
        if (k < array.length) {
            array[k] = num;
        } else {
            array[array.length - 1] = num;
        }
        arrayOddNumbers(array);
    }

    //TASK 1.4.2
    public static void arrayOddNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i] + ", ");
            }
        }
        System.out.println();
    }

    //TASK 1.5.1
    public static void arrayXY(int[] array, int k, int l) {
        for (int i = l; i < array.length && k > 0; i++, k--) {
            array[i] = 0;
        }
        array20(array);
    }

    //TASK 1.5.2
    public static void array20(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 20 == 0) {
                System.out.println("|" + array[i] + "|, ");
            }
        }
    }

    public static void arrayPrint(int[] array1, int[] array2) {
        for (int i = 0, diff = 0; i < array1.length && i < array2.length; i++) {
            if (diff == 0 && array1[i] != array2[i]) {
                System.out.println("array1[" + i + "] = " + array1[i] + "\t!= array2[" + i + "] = " + array2[i]);
                diff++;
            } else {
                System.out.println("array1[" + i + "] = " + array1[i] + "\t~~ array2[" + i + "] = " + array2[i]);
            }
        }
    }

    public static void arrayPrint(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("array[" + i + "] = " + array[i]);
        }
    }
}
